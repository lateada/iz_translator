﻿iz

Welcome to the “iz” Translator! 

The Translator will accept single or multiple word entries 
and return the ‘iz’ version.

Tracing its roots to the Harlem Renaissance, “iz” is an estotric language game 
similar to Pig Latin and Izzle. The general rules for “iz” are as follows:

1. 	For words which begin with a single consonant , “iz” is placed between the 	
    intial consonant and before the initial vowel.
	
	    Ex: 	bad → bizad
		        damn → dizamn
		

2. 	For words that begin with vowels (a,e,i,o,u), “iz" is added to the 
    front of the word:

        Ex: 	apple → izapple
	            uncle → izuncle

3. 	For words that begin with consonant sounds (multiple consonants), “iz” is 	
    placed between the initial consonant sounds and before the initial vowel.

        Ex:	    crazy → crizazy”, 
	            what → whizat

When prompted, enter a word or phraze (no symbols or numbers, please) and 
check out the results. I have included standard and regular expression versions 
for both Python 2 and Python 3.

Have fizun!


For additional information about 'iz' see:

    i.  Lindsay, Mark, “American English iz‐Infixation: interaction of phonology, 
        metrics, and rhyme”, Stony Brook University, 

        https://linguistics.stonybrook.edu/sites/default/files/uploads/u3/
        lindsay_iz-infixation.pdf 

	ii. Janice Hansen, “Tizhe Lizanguage bizof Lizovers: Carny Latin 
	    Reincarnated”,The Devil’s Tale, Dispatches from the David M. Rubenstein 
	    Rare Book & Manuscript Library at Duke University,

        https://blogs.library.duke.edu/rubenstein/2014/12/17/
        tizhe-lizanguage-bizof-lizovers-carny-latin-reincarnated/