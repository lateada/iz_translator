"""IZ TRANSLATOR - PYTHON THREE"""
"""converts English text to 'iz'
by la januari"""

print ("Welcome to the 'iz' Translator!" +
    " \n\nLet's translate(...no numbers or symbols please!)")

def using_or():

    print()
    original = input("Please enter word(s): ")
    words = original.lower()
#Separates text into a list
    word = words.split()
    vowels = ("aeiou")
    zzz = "iz"
    phraze = []

#check for empty value entry
    if len(words) == 0:
        print ("\nOops! You did not enter anything... \n"
        "Try again!")
        using_or()


    for wrd in word:
#check for single non-vowel entry
        if (len(wrd) == 1) and (wrd not in vowels) and \
        (wrd.isalpha()) and not (wrd.isdigit()):
            print ("\nPlease enter whole English words!" +
            "\nTry again!")
            using_or()


        elif len(wrd) > 0 and wrd.isalpha():

            i = 0
            while len (wrd) > i:
                if wrd[i] in vowels:
                    break
#check for y as a vowel
                if i > 0 and wrd[i] in "y":
                    break
                i += 1
            if i < len(wrd):
                back = wrd[i:]
                front = wrd [:i]

                new_word = front + zzz + back
                print (new_word, end=" "),

#check for multi-letter entry without vowels or y
            else:
                print ("\nHmmm...English words have vowels or vowel sounds...\n"
                "Try again!")
                using_or()

#check for non-letter entry
        else:
            print ("\nRemember to only enter alphabet letters...\n"
            "Try again!\n")
            using_or()

using_or()

def play_again():
    question = input("\n\nWould you like to play once more? y or n: ")
    text = question.lower()
    if text == "y":
        using_or()
        play_again()

    elif text == "n":
        print ("\nGoodbye! Thanks for playing!!!")
    else:
        print ("\nWell . . . Adios!")


play_again()
